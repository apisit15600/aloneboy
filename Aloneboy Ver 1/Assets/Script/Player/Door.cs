﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Door : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
    void OnTriggerEnter(Collider go)
    {
        if(go.GetComponent<Collider>().tag == "Doors")
        {
             SceneManager.LoadScene ("SceneGameplay");
        }
    }
}
