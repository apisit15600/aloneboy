﻿using UnityEngine;

public class GUN : MonoBehaviour {
    public int damage = 10;
    public float rang = 100f;
    public float impactForce = 30f;
    public float fireRate = 15f;
    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public GameObject impactEffect ;
    public GameObject LightGun ;
    public AudioSource shotAudio ;
    bool light ;
    private float nextTimeToFire = 0f;
    void Start () {
    }

    void Update () {
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire) 
        {
           
           nextTimeToFire = Time.time + 1f/fireRate;
            Shoot ();

        }
        if(Input.GetKeyDown(KeyCode.F))
        {
            light =! light;

        }
        if(light)
           LightGun.SetActive(true);
        else
        LightGun.SetActive(false);
        
    }
    void Shoot () {
        muzzleFlash.Play();
        shotAudio.Play();
        RaycastHit hit;
        if (Physics.Raycast (fpsCam.transform.position, fpsCam.transform.forward, out hit, rang)) {
            Debug.Log (hit.transform.name);
            EnemyHealth enemyHealth = hit.transform.GetComponent<EnemyHealth> ();
            if (enemyHealth != null) {
                enemyHealth.TakeDamage (damage);
            }
            if(hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce) ;
            }
            GameObject impactGO = Instantiate(impactEffect,hit.point,Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 0.1f);
        }
    }
}