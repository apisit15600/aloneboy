﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {
    public int startingHealth = 100;
    public Slider healthSlider;
    public Image damageImage;
    public AudioClip deathClip;
    public float flashSpeed = 5f;
    public Color flashColour = new Color (1f, 0f, 0f, 0.1f);
    AudioSource playerAudio;
    public int currentHealth ;


    GUN gun;
    bool isDead;
    bool damaged;

    void Awake () {

        playerAudio = GetComponent<AudioSource> ();
        gun = GetComponentInChildren<GUN> ();

        currentHealth = startingHealth;
    }

    void Update () {

        if (damaged) {

            damageImage.color = flashColour;
        } else {

            damageImage.color = Color.Lerp (damageImage.color, Color.clear, flashSpeed * Time.deltaTime);
        }

        damaged = false;
         if(Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("SceneMainMenu");
            }
    }

    public void TakeDamage (int amount) {

        damaged = true;

        currentHealth -= amount;

        healthSlider.value = currentHealth;

        playerAudio.Play ();

        if (currentHealth <= 0 && !isDead) {

            Death ();
        }
    }

    void Death () {
        isDead = true;
        playerAudio.clip = deathClip;
        playerAudio.Play ();
        gun.enabled = false;
    }

}