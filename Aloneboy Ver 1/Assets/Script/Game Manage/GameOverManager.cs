using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

    public class GameOverManager : MonoBehaviour
    {
        ZombieScoreManager zombieScore ;
        ScoreManager scoremanager;
        public GameObject HUD ;
        public GameObject gameOver ;
        public PlayerHealth playerHealth; 
        public Text scoreOver;  
        public Text zombieOver ;  
        int Overscore;                 
        void Awake ()
        {

        }


        void Update ()
        {
           
            if(playerHealth.currentHealth <= 0)
            {
                gameOver.SetActive(true) ;
                HUD.SetActive(false);
                if(Input.GetKeyDown(KeyCode.F))
            {
                SceneManager.LoadScene("SceneGameplay");
            }
             if(Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene("SceneMainMenu");
            }
            }
            scoreOver.text = "Score : " + ScoreManager.score ;
            zombieOver.text = "Zombie : " + ZombieScoreManager.zombieScore ;

        }
   
}
