﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


    public class ZombieScoreManager : MonoBehaviour
    {
        public static int zombieScore;       
        Text text; 
        void Awake ()
        {
            text = GetComponent <Text> ();

            zombieScore = 0;
        }
        void Update ()
        {
            text.text = "Zombie : " + zombieScore;
        }
    }
