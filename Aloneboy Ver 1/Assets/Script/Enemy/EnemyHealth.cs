﻿using UnityEngine;

public class EnemyHealth : MonoBehaviour {
    public int startingHealth = 100;
    public int currentHealth;
    public float sinkSpeed = 2.5f;
    public int scoreValue = 10;
    public int Zombie = 1 ;
    public AudioClip deathClip;
    ScoreManager scoreManager;
    ZombieScoreManager zombieScoreManager ;
    Animator anim;
    AudioSource enemyAudio;
    CapsuleCollider capsuleCollider;
    public ParticleSystem _blood;
    bool isDead;
    bool isSinking;

    void Awake () {

        anim = GetComponent<Animator> ();
        enemyAudio = GetComponent<AudioSource> ();
        capsuleCollider = GetComponent<CapsuleCollider> ();
        currentHealth = startingHealth;
    }

    void Update () {

        if (isSinking) {

            transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
        }
       
    }

    public void TakeDamage (int amount) {

        if (isDead)
            return;
        enemyAudio.Play ();
        currentHealth -= amount;
        _blood.Play();
        if (currentHealth <= 0) {
            Death ();
        }
    }

    void Death () {

        isDead = true;

        capsuleCollider.isTrigger = true;

        anim.SetTrigger ("Dead");
        ScoreManager.score += scoreValue;
        ZombieScoreManager.zombieScore += Zombie ;
        enemyAudio.clip = deathClip;
        enemyAudio.Play ();
        
        Destroy (gameObject,2f);
    }

    public void StartSinking () {

        GetComponent<UnityEngine.AI.NavMeshAgent> ().enabled = false;

        GetComponent<Rigidbody> ().isKinematic = true;

        isSinking = true;
    }
}